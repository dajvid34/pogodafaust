package com.example.hp.pogoda;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListaFragment extends Fragment{
    View view;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    List<Continent> continents=new ArrayList<Continent>();
    List<String> test=new ArrayList<String>();
    ListView lista;
    String tmp;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view=inflater.inflate(R.layout.activity_main,container,false);
        if(test.isEmpty()) {
            makeElements();
            for (int i = 0; i < continents.size(); i++) {
                test.add(continents.get(i).name);
            }
        }
        lista=(ListView) view.findViewById(R.id.lista);
        lista.setAdapter(new TestAdapter(test));

        lista.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
                tmp = test.get(position);
                test.clear();
                addContinents(tmp);
                lista.setAdapter(new TestAdapter(test));
            }
        });
        return view;
    }

    class TestAdapter extends BaseAdapter{
        private List<String> elem;

        public TestAdapter(List<String> test){
            elem=test;
        }

        @Override
        public int getCount() {
            return elem.size();
        }

        @Override
        public String getItem(int position) {
            return elem.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView res;
            if(convertView==null){
                res=new TextView(parent.getContext());
                res.setTextSize(30);
                res.setText(elem.get(position));
            }
            else
            {
                res=(TextView)convertView;
                res.setTextSize(30);
                res.setText(elem.get(position));
            }
            return res;
        }
    }

    private void change(){
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            ConFragment2 frag=(ConFragment2) getFragmentManager().findFragmentById(R.id.fragment2);
            frag.setTytul(tmp);
        }
        else{
            getFragmentManager().beginTransaction().replace(R.id.container, new ConFragment(tmp)).addToBackStack(null).commit();
        }
    }

    private void addContinents(String n){
        for(int i=0;i<continents.size();i++){
            test.add(continents.get(i).name);
            if(continents.get(i).expand){
                if(continents.get(i).name==n){
                    continents.get(i).setExpandFalse();
                    continue;
                }
                else
                {
                    addCountries(i,n);
                }
            }
            else{
                if(continents.get(i).name==n){
                    continents.get(i).setExpandTrue();
                    addCountries(i,n);
                }
            }
        }
    }

    private void addCountries(int i,String n){
        for(int j=0;j<continents.get(i).countries.size();j++){
            test.add("\t"+continents.get(i).countries.get(j).name);

            if(continents.get(i).countries.get(j).expand){
                if(n.equals("\t"+continents.get(i).countries.get(j).name)){
                    continents.get(i).countries.get(j).setExpandFalse();
                    continue;
                }
                else
                {
                    addCities(i,j,n);
                }
            }
            else
            {
                if(n.contains(continents.get(i).countries.get(j).name)){
                    continents.get(i).countries.get(j).setExpandTrue();
                    addCities(i,j,n);
                }
            }
        }
    }

    private void addCities(int i,int j, String n){
        for(int k=0;k<continents.get(i).countries.get(j).cities.size();k++){
            test.add("\t\t"+continents.get(i).countries.get(j).cities.get(k).name);
            if(n.equals("\t\t"+continents.get(i).countries.get(j).cities.get(k).name)){
                change();
            }
        }
    }

    void makeElements(){
        continents.add(new Continent("Afryka"));
        continents.add(new Continent("Azja"));
        continents.add(new Continent("Europa"));

        continents.get(0).addCountry("Egipt");
        continents.get(0).addCountry("Maroko");
        continents.get(0).addCountry("RPA");

        continents.get(1).addCountry("Chiny");
        continents.get(1).addCountry("Indonezja");
        continents.get(1).addCountry("Turcja");

        continents.get(2).addCountry("Grecja");
        continents.get(2).addCountry("Polska");
        continents.get(2).addCountry("Włochy");

        continents.get(0).countries.get(0).addCity("Aleksandria");
        continents.get(0).countries.get(0).addCity("Kair");
        continents.get(0).countries.get(0).addCity("Suez");

        continents.get(0).countries.get(1).addCity("Casablanca");
        continents.get(0).countries.get(1).addCity("Rabat");
        continents.get(0).countries.get(1).addCity("Tanger");

        continents.get(0).countries.get(2).addCity("Bloemfontein");
        continents.get(0).countries.get(2).addCity("Johannesburg");
        continents.get(0).countries.get(2).addCity("Pretoria");

        continents.get(1).countries.get(0).addCity("Chengdu");
        continents.get(1).countries.get(0).addCity("Pekin");
        continents.get(1).countries.get(0).addCity("Wuhan");

        continents.get(1).countries.get(1).addCity("Bandung");
        continents.get(1).countries.get(1).addCity("Jakarta");
        continents.get(1).countries.get(1).addCity("Medan");

        continents.get(1).countries.get(2).addCity("Ankara");
        continents.get(1).countries.get(2).addCity("Izmir");
        continents.get(1).countries.get(2).addCity("Stambul");

        continents.get(2).countries.get(0).addCity("Ateny");
        continents.get(2).countries.get(0).addCity("Pireus");
        continents.get(2).countries.get(0).addCity("Saloniki");

        continents.get(2).countries.get(1).addCity("Bydgoszcz");
        continents.get(2).countries.get(1).addCity("Lodz");
        continents.get(2).countries.get(1).addCity("Warszawa");

        continents.get(2).countries.get(2).addCity("Bolonia");
        continents.get(2).countries.get(2).addCity("Neapol");
        continents.get(2).countries.get(2).addCity("Rzym");

    }
}
