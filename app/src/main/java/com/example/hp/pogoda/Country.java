package com.example.hp.pogoda;

import java.util.ArrayList;
import java.util.List;

public class Country {
    String name;
    boolean expand=false;
    List<City> cities=new ArrayList<City>();

    public Country(String name){
        this.name=name;
    }

    void addCity(String name){
        cities.add(new City(name));
    }

    public void setExpandTrue(){
        expand=true;
    }

    public void setExpandFalse(){
        expand=false;
    }
}
