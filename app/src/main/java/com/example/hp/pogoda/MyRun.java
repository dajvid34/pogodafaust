package com.example.hp.pogoda;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

class MyRun implements Runnable {

    private String adres;
    private String Strona = "";
    InputStream inputStream = null;

    public MyRun(String adres) {
        this.adres = adres;
    }

    @Override
    public synchronized void run() {
        try {
            URL url = new URL(adres);
            //create the new connection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            //set up some things on the connection
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            //and connect!
            urlConnection.connect();
            //this will be used in reading the data from the internet
            inputStream = urlConnection.getInputStream();
            Strona = readIt(inputStream, 500);
        } catch (Throwable e) {
            Strona = "Wait...";
            e.printStackTrace();
        }
    }

    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    public String get() {
        System.out.println(Strona);
        return Strona;
    }
}