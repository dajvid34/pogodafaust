package com.example.hp.pogoda;

import java.util.ArrayList;
import java.util.List;

public class Continent {
    String name;
    boolean expand=false;
    List<Country> countries=new ArrayList<Country>();

    public Continent(String name){
        this.name=name;
    }

    void addCountry(String name){
        countries.add(new Country(name));
    }

    public void setExpandTrue(){
        expand=true;
    }

    public void setExpandFalse(){
        expand=false;
    }
}
