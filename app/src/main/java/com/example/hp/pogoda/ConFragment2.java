package com.example.hp.pogoda;

import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class ConFragment2 extends Fragment {
    View view;
    ImageView image;
    private static TextView textview,textview2,textview3,textview4,textviewTytul,textviewBrakNeta;
    String url, tytul, strona2;

   public void setTytul(String q){
       tytul=q;
       image = (ImageView) view.findViewById(R.id.imageView1);
       boolean isNetAvailable = isNetworkAvailable();

       if (tytul == null) {
           tytul = "pppp";
       }

       url = "http://api.openweathermap.org/data/2.5/weather?q=" + tytul.substring(2);

       textviewBrakNeta = (TextView) view.findViewById(R.id.brakNeta);
       textview = (TextView) view.findViewById(R.id.temp);
       textview2 = (TextView) view.findViewById(R.id.speed);
       textview3 = (TextView) view.findViewById(R.id.press);
       textview4 = (TextView) view.findViewById(R.id.clouds);
       textviewTytul = (TextView) view.findViewById(R.id.tytul);
       textviewTytul.setText(tytul);

       if (isNetAvailable) {
           textviewTytul.setText(tytul);
           textviewBrakNeta.setText("");
           MyRun myRun = new MyRun(url);
           Thread watek = new Thread(myRun);
           watek.start();
           try {
               watek.join();
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
           strona2 = myRun.get();
           try {
               InternalStorage.writeObject(this.getActivity(), "jstring", strona2);
           } catch (IOException e) {
               e.printStackTrace();
           }
           doJSON(strona2);
       } else {
           textviewBrakNeta.setText("Brak dostępu do Internetu. \nOstatnie pobrane dane");
           try {
               String jstring = (String) InternalStorage.readObject(this.getActivity(), "jstring");
               doJSON(jstring);
           } catch (IOException e) {
               e.printStackTrace();
           } catch (ClassNotFoundException e) {
               e.printStackTrace();
           }
       }
   }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pogoda, container, false);
        return view;
    }
    public void doJSON(String strona2){
        if(strona2!=""&&strona2!="Wait...")
        {
            JSONObject strona;
            try{
                strona = new JSONObject(strona2);
                JSONArray arr = strona.getJSONArray("weather");

                String pageName2 = arr.getJSONObject(0).getString("description");
                String pageName6 = arr.getJSONObject(0).getString("icon");
                String pageName3 = strona.getJSONObject("main").getString("temp");
                String pageName4 = strona.getJSONObject("main").getString("pressure");
                String pageName5 = strona.getJSONObject("wind").getString("speed");

                changeIcon("im"+pageName6);

                float amount=Float.parseFloat(pageName3);
                float cel=amount-273.16f;
                String result = String.format("%.1f", cel);
                String tic=String.valueOf(result);

                textview.setText("Weather:"+pageName2);
                textview2.setText("Temp: "+tic+" °C");
                textview3.setText("Pressure: "+pageName4+" hPa");
                textview4.setText("Wind: "+pageName5+" m/s");
            }catch (JSONException e){
                e.printStackTrace();
                textview2.setText("Nie odnaleziono miasta");
            }
        }
        else
        {
            textview.setText("Wystąpił błąd. \nSpróbuj ponownie");
            textview2.setText("");
            textview3.setText("");
            textview4.setText("");
            changeIcon("im00");
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void changeIcon(String q){
        if (q.equals("im00"))
            image.setImageResource(R.drawable.im00);
        if (q.equals("im01d"))
            image.setImageResource(R.drawable.im01d);
        if (q.equals("im01n"))
            image.setImageResource(R.drawable.im01n);
        if (q.equals("im02d"))
            image.setImageResource(R.drawable.im02d);
        if (q.equals("im02n"))
            image.setImageResource(R.drawable.im02n);
        if (q.equals("im03d"))
            image.setImageResource(R.drawable.im03d);
        if (q.equals("im03n"))
            image.setImageResource(R.drawable.im03n);
        if (q.equals("im04d"))
            image.setImageResource(R.drawable.im04d);
        if (q.equals("im04n"))
            image.setImageResource(R.drawable.im04n);
        if (q.equals("im09d"))
            image.setImageResource(R.drawable.im09d);
        if (q.equals("im09n"))
            image.setImageResource(R.drawable.im09n);
        if (q.equals("im10d"))
            image.setImageResource(R.drawable.im10d);
        if (q.equals("im10n"))
            image.setImageResource(R.drawable.im10n);
        if (q.equals("im11d"))
            image.setImageResource(R.drawable.im11d);
        if (q.equals("im11n"))
            image.setImageResource(R.drawable.im11n);
        if (q.equals("im13d"))
            image.setImageResource(R.drawable.im13d);
        if (q.equals("im13n"))
            image.setImageResource(R.drawable.im13n);
        if (q.equals("im50d"))
            image.setImageResource(R.drawable.im50d);
        if (q.equals("im50n"))
            image.setImageResource(R.drawable.im50n);
    }
}
